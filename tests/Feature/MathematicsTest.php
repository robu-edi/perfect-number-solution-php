<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Http\Controllers\MathematicsController;

class MathematicsTest extends TestCase
{

    /**
     * Test if  abundant number
     *
     */
    public function testPerfectNumberAbundant()
    {

        $response = $this->json('GET', '/api/v1/mathematics/number-classification/12');

        $response
            ->assertStatus(200)
            ->assertExactJson([
                'status'    => 'Success',
                'message'   => 'Abundant'
            ]);
    }

    /**
     * Test if  deficient number
     *
     */
    public function testPerfectNumberDeficient()
    {
        $response = $this->json('GET', '/api/v1/mathematics/number-classification/8');

        $response
            ->assertStatus(200)
            ->assertExactJson([
                'status'    => 'Success',
                'message'   => 'Deficient'
            ]);
    }

    /**
     * Test if  perfect number
     *
     */
    public function testPerfectNumberPerfect()
    {

        $response = $this->json('GET', '/api/v1/mathematics/number-classification/6');

        $response
            ->assertStatus(200)
            ->assertExactJson([
                'status'    => 'Success',
                'message'   => 'Perfect'
            ]);


    }

    /**
     * Test if error on double number
     *
     */
    public function testPerfectNumberDoubleValue()
    {
        $response = $this->json('GET', '/api/v1/mathematics/number-classification/6.5');

        $response
            ->assertStatus(200)
            ->assertExactJson([
                'status'    => 'Error',
                'message'   => 'Invalid input'
            ]);
    }

    /**
     * Test if error on string number
     *
     */
    public function testPerfectNumberStringValue()
    {
        $response = $this->json('GET', '/api/v1/mathematics/number-classification/six');

        $response
            ->assertStatus(200)
            ->assertExactJson([
                'status'    => 'Error',
                'message'   => 'Invalid input'
            ]);
    }
}
